<div id="main">
<div class="inner">
    <?php get_template_part( 'template-parts/content/primary' ); ?>
    <?php get_template_part( 'sidebar' ); ?>
    <?php get_template_part( 'template-parts/pagination' ); ?>
    <div class="clear"></div>
</div><!-- inner -->
</div><!-- main -->
